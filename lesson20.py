# class User:
#     name = 'Artem'
#     age = 40
#
#     msg = f'Hello! I\'m Artem, 40'
#
#     def __init__(self, initial_name, initial_age):
#         self.name = initial_name
#         self.age = initial_age
#
#         self.msg = f'Hello! I\'m {self.name}, {self.age}'
#
#     def hello_message(self):
#         return f'Hello! I\'m {self.name}, {self.age}'
#
#
# user = User('My name', 30)
#
# print(user.name)
# print(user.age)
#
# user.age = 20
#
# print(user.hello_message())
# print(user.msg)

#
# class User:
#     name = 'Artem'
#     age = 40
#
#     def __init__(self, initial_name, initial_age):
#         self.name = initial_name
#         self.age = initial_age
#
#     def msg_getter(self):
#         print('In getter')
#         return f'Hello! I\'m {self.name}, {self.age}'
#
#     def msg_setter(self, value):
#         print('In setter', value)
#
#     msg = property(msg_getter, msg_setter)
#
#
# user = User('My name', 30)
#
# print(user.msg)
# user.msg = 30
# user.msg = 50


# class User:
#     name = 'Artem'
#     _age = None
#
#     def __init__(self, initial_name, initial_age):
#         self.name = initial_name
#         self.age = initial_age
#
#     def age_getter(self):
#         print('In getter')
#         return self._age
#
#     def age_setter(self, value):
#         print('In setter')
#         if type(value) != int:
#             raise TypeError('Age must be an integer')
#         self._age = value
#
#     age = property(age_getter, age_setter)
#
#
# user = User('My name', 12)
#
# print(user.age)


# class User:
#     name = 'Artem'
#     _age = None
#
#     def __init__(self, initial_name, initial_age):
#         self.name = initial_name
#         self.age = initial_age
#
#     age = property()
#
#     @age.getter
#     def age(self):
#         print('In getter')
#         return self._age
#
#     @age.setter
#     def age(self, value):
#         print('In setter')
#         if type(value) != int:
#             raise TypeError('Age must be an integer')
#         self._age = value
#
#
# user = User('My name', 12)
#
# print(user.age)


# class User:
#     name = 'Artem'
#     _age = None
#
#     def __init__(self, initial_name, initial_age):
#         self.name = initial_name
#         self.age = initial_age
#
#     @property
#     def age(self):  # .age() -> .age
#         print('In getter')
#         return self._age
#
#     @age.setter
#     def age(self, value):
#         print('In setter')
#         if type(value) != int:
#             raise TypeError('Age must be an integer')
#         self._age = value
#
#
# user = User('My name', 12)
#
# print(user.age)
#
# user.age = 123


# class User:
#     name = 'Artem'
#     age = 40
#
#     def __init__(self, initial_name, initial_age):
#         self.name = initial_name
#         self.age = initial_age
#
#     @property
#     def msg(self):
#         return f'Hello! I\'m {self.name}, {self.age}'
#
#
# user = User('SomeName', 123)
#
# print(user.msg)
#
# user.age = 31
#
# print(user.msg)


# class User:
#     name = 'Artem'
#     _age = None
#
#     def __init__(self, initial_name, initial_age):
#         self.name = initial_name
#         self.age = initial_age
#
#     @property
#     def age(self):  # .age() -> .age
#         print('In getter')
#         return self._age
#
#     @age.setter
#     def age(self, value):
#         print('In setter')
#         if type(value) != int:
#             raise TypeError('Age must be an integer')
#         self._age = value
#
#
# user = User('My name', 12)
#
# print(user.age)
#
# user.age = 123


class Point:
    _x = None
    _y = None

    def __init__(self, x, y):
        self.x = x
        self.y = y

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, value):
        if not isinstance(value, (int, float)):
            raise TypeError
        self._x = value

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, value):
        if not isinstance(value, (int, float)):
            raise TypeError
        self._y = value


class Line:
    # todo: HW check type Point
    begin = None
    end = None

    def __init__(self, point_b, point_e):
        self.begin = point_b
        self.end = point_e

    def get_length(self):
        return (((self.begin.x - self.end.x) ** 2) + ((self.begin.y - self.end.y) ** 2)) ** 0.5

    @property
    def length(self):
        return (((self.begin.x - self.end.x) ** 2) + ((self.begin.y - self.end.y) ** 2)) ** 0.5


class Triangle:
    # todo: HW check type Point
    point_a = None
    point_b = None
    point_c = None

    def __init__(self, point_a, point_b, point_c):
        self.point_a = point_a
        self.point_b = point_b
        self.point_c = point_c

    @property
    def area(self):
        line_a = Line(self.point_a, self.point_b).length
        line_b = Line(self.point_b, self.point_c).length
        line_c = Line(self.point_c, self.point_a).length

        sp = (line_a + line_b + line_c) / 2

        res = (sp * (sp-line_a) * (sp-line_b) * (sp-line_c)) ** 0.5

        return res


p1 = Point(0, 3)
p2 = Point(4, 0)


line = Line(p1, p2)

print(line.get_length())
print(line.length)


line.begin = Point(10, 10)
print(line.length)


tr = Triangle(Point(0, 0), Point(0, 5), Point(5, 0))

print(tr.area)
