# abstractions

# dealer
# player

# hand for dealer  -> some cards
# hand for player

# deck of cards

# card (value)


# functions

# shaffle the deck

# calculate points hand

# add cards into hand form the deck

# game logic for dealer

# visualisation for dealer hand

# visualisation for player hand

# user interaction


import random

deck_template_52_card = {
    '2♠': (2, 2),
    '3♠': (3, 3),
    '4♠': (4, 4),
    '5♠': (5, 5),
    '6♠': (6, 6),
    '7♠': (7, 7),
    '8♠': (8, 8),
    '9♠': (9, 9),
    '10♠': (10, 10),
    'J♠': (10, 10),
    'Q♠': (10, 10),
    'K♠': (10, 10),
    'A♠': (11, 1),
    '2♥': (2, 2),
    '3♥': (3, 3),
    '4♥': (4, 4),
    '5♥': (5, 5),
    '6♥': (6, 6),
    '7♥': (7, 7),
    '8♥': (8, 8),
    '9♥': (9, 9),
    '10♥': (10, 10),
    'J♥': (10, 10),
    'Q♥': (10, 10),
    'K♥': (10, 10),
    'A♥': (11, 1),
    '2♣': (2, 2),
    '3♣': (3, 3),
    '4♣': (4, 4),
    '5♣': (5, 5),
    '6♣': (6, 6),
    '7♣': (7, 7),
    '8♣': (8, 8),
    '9♣': (9, 9),
    '10♣': (10, 10),
    'J♣': (10, 10),
    'Q♣': (10, 10),
    'K♣': (10, 10),
    'A♣': (11, 1),
    '2♦': (2, 2),
    '3♦': (3, 3),
    '4♦': (4, 4),
    '5♦': (5, 5),
    '6♦': (6, 6),
    '7♦': (7, 7),
    '8♦': (8, 8),
    '9♦': (9, 9),
    '10♦': (10, 10),
    'J♦': (10, 10),
    'Q♦': (10, 10),
    'K♦': (10, 10),
    'A♦': (11, 1),
}


def make_game_deck(deck_tpl: dict[str, tuple[int, int]], number_of_decks: int) -> list[tuple[str, tuple[int, int]]]:
    deck_of_cards: list[tuple[str, tuple[int, int]]] = []

    for _ in range(number_of_decks):
        deck_of_cards += list(deck_tpl.items())

    random.shuffle(deck_of_cards)

    return deck_of_cards


def get_cards_from_deck(deck: list, cards_number: int) -> list:
    """Changes the size of the deck implicitly!"""

    res: list[tuple[str, tuple[int, int]]] = []

    for _ in range(cards_number):
        res.append(deck.pop())

    return res


def calc_hand(hand: list[tuple[str, tuple[int, int]]]) -> tuple[int, int]:

    res1: int = 0
    res2: int = 0
    for card_name, card_value in hand:
        res1 += card_value[0]
        res2 += card_value[1]

    return res1, res2


game_deck = make_game_deck(
    deck_tpl=deck_template_52_card,
    number_of_decks=4,
)


# todo: HW: -1-> put it into function `game`

# todo: HW: -> define conditions of ending the game
# while len(deck) > x:

# todo: HW: -2-> put it into function `round_of_play`

dealer_hand: list[tuple[str, tuple[int, int]]] = []
user_hand: list[tuple[str, tuple[int, int]]] = []

for _ in range(2):
    user_hand.extend(get_cards_from_deck(game_deck, 1))
    dealer_hand.extend(get_cards_from_deck(game_deck, 1))

print(dealer_hand)
print(calc_hand(dealer_hand))

print(user_hand)
print(calc_hand(user_hand))

# todo: HW: make function for player (whether to add cards or not)
# todo: HW: make function for dealer (whether to add cards or not)
# todo: HW: make function to define the winner

# <-2-

# <-1-
