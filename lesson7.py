# functions

# 1 DRY and KISS
# 2 isolation


# data input

# process data

# output data


# def function_name(arg1, arg2):
#     print('arg1', arg1)
#     print('arg2', arg2)
#
#     return arg1 + arg2
#
#
# res = function_name(1, 2)
# print('-->', res)
# res = function_name(3, 4)
# print('-->', res)


# def get_integer(prompt):
#     while True:
#         try:
#             return int(input(prompt))
#         except:
#             print('Not a number')
#
#
# def cashier_answer(age):
#
#     msg = "We are out of tickets"
#     if age <= 0:
#         msg = "Invalid"
#     elif "7" in str(age):
#         msg = 'You will find your luck'
#     elif age < 6:
#         msg = "Where are your parents?"
#     elif age < 16:
#         msg = "This film is 18+ category"
#     elif age >= 60:
#         msg = "Do you have your pension ID?"
#
#     return msg
#
#
# def cinema():
#     user_age = get_integer("How old are you: ")
#
#     answer = cashier_answer(user_age)
#
#     print(answer)
#
#
# cinema()


# def function_name(arg1, arg2):
#     print('arg1', arg1)
#     print('arg2', arg2)
#
#     return arg1 + arg2
#
#
# res = function_name(2, 1)  # positional
#
# res = function_name(arg1=2, arg2=1)  # named
# res = function_name(arg2=1, arg1=2)  # named
# res = function_name(arg2=1, arg1=2)  # named
# res = function_name(arg2=1, arg1=2)  # named


# def function_name(arg1, arg2, arg3):
#     print('arg1', arg1)
#     print('arg2', arg2)
#     print('arg3', arg3)
#
#     return arg1 + arg2

#
# res = function_name(1, 2, 3)
# res = function_name(arg2=1, arg1=2, arg3=10)


# res = function_name(1, arg3=2, arg2=10)


# def function_name(arg1, arg2, arg3=0):
#     print('arg1', arg1)
#     print('arg2', arg2)
#     print('arg3', arg3)
#
#     return arg1 + arg2
#
#
# res = function_name(1, 10)


# def function_name(arg1=1, arg2=2, arg3=3):
#     print('arg1', arg1)
#     print('arg2', arg2)
#     print('arg3', arg3)
#
#     return arg1 + arg2
#
#
# res = function_name(arg3=10)


# def function_name(value, list_to_add=[]):
#     list_to_add.append(value)
#     return list_to_add
#
#
# lst = [1, 2, 3]
#
# res = function_name(4, lst)
#
# print(res)
#
# res = function_name(4)
# print(res)
#
# res = function_name(5)
# print(res)
#
# res = function_name('asdfg')
# print(res)


def function_name(value, list_to_add=None):

    if list_to_add is None:
        list_to_add = []

    list_to_add.append(value)
    return list_to_add


lst = [1, 2, 3]

res = function_name(4, lst)

print(res)

res = function_name(4)
print(res)

res = function_name(5)
print(res)

res = function_name('asdfg')
print(res)

"""
1. Напишіть функцію, що приймає один аргумент будь-якого типу та повертає цей аргумент, перетворений на float. 
Якщо перетворити не вдається функція має повернути 0.

2. Напишіть функцію, що приймає два аргументи. Функція повинна
якщо аргументи відносяться до числових типів (int, float) - повернути перемножене значення цих аргументів,
якщо обидва аргументи це строки (str) - обʼєднати в одну строку та повернути
у будь-якому іншому випадку повернути кортеж з цих аргументів

3. Перепишіть за допомогою функцій вашу программу "Касир в кінотеатрі", яка буде виконувати наступне:
Попросіть користувача ввести свсвій вік.
- якщо користувачу менше 7 - вивести "Тобі ж <> <>! Де твої батьки?"
- якщо користувачу менше 16 - вивести "Тобі лише <> <>, а це е фільм для дорослих!"
- якщо користувачу більше 65 - вивести "Вам <> <>? Покажіть пенсійне посвідчення!"
- якщо вік користувача містить 7 - вивести "Вам <> <>, вам пощастить"
- у будь-якому іншому випадку - вивести "Незважаючи на те, що вам <> <>, білетів всеодно нема!"
Замість <> <> в кожну відповідь підставте значення віку (цифру) та правильну форму слова рік. 
Для будь-якої відповіді форма слова "рік" має відповідати значенню віку користувача 
(1 - рік, 22 - роки, 35 - років і тд...). 

Наприклад :

"Тобі ж 5 років! Де твої батьки?"
"Вам 81 рік? Покажіть пенсійне посвідчення!"
"Незважаючи на те, що вам 42 роки, білетів всеодно нема!"


"""