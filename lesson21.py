# # descriptor
#
# class OnlyIntDescriptor:
#
#     _name = ''
#
#     def __init__(self, name):
#         self._name = name
#
#     def __set__(self, instance, value):
#         if not isinstance(value, int):
#             raise TypeError
#         instance.__dict__[self._name] = value
#
#     def __get__(self, instance, owner):
#         return instance.__dict__[self._name]
#
#     def __delete__(self, instance):
#         del instance.__dict__[self._name]
#
#
# class Point:
#     x = OnlyIntDescriptor('x')
#     y = OnlyIntDescriptor('y')
#
#     def __init__(self, x, y):
#         self.x = x
#         self.y = y
#
#
# p = Point(12345, 132)
#
#
# print(p.__dict__)
#
# del p.y
#
# print(p.__dict__)


# abstract classes

#
# class Vehicle:
#
#     def drive(self):
#         return self._drive()
#
#     def _drive(self):
#         raise NotImplementedError
#
#     def stop(self):
#         return self._stop()
#
#     def _stop(self):
#         raise NotImplementedError
#
#
# class Car(Vehicle):
#
#     def _drive(self):
#         print('drive by car')
#
#     def _stop(self):
#         print('stop by car')
#
#
# c = Car()
#
# c.drive()
# c.stop()


from abc import ABC, abstractmethod


class Vehicle(ABC):

    @abstractmethod
    def drive(self):
        pass

    @abstractmethod
    def stop(self):
        pass


class Car(Vehicle):

    def drive(self):
        print('drive by car')

    # def stop(self):
    #     print('stop by car')


c = Car()
c.drive()
c.stop()
