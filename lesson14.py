# class User:  # class User(object):
#     name = None
#     age = None
#     city = 'a'
#
#     def __init__(self, new_age=None, new_name=None):
#         if new_age is not None:
#             self.age = new_age
#
#         if new_name is not None:
#             self.name = new_name
#
#     def say_hello(self):
#         print(f'Hello, I\'m {self.name}, I\'m {self.age} years old')
#
#
# class Programmer(User):
#     language = None
#     city = 'b'
#
#     def __init__(self, new_age, new_name, lang):
#         self.age = new_age
#         self.name = new_name
#         self.language = lang
#
#     def say_hello(self):
#         print(f'Hello, I\'m programmer {self.name}, I\'m {self.age} years old, I use {self.language}')
#
#
# u1 = User(40, 'Artem_programmer')
# u1.say_hello()
#
#
# p1 = Programmer(40, 'Artem_programmer', 'python')
# p1.say_hello()
#
#
# print(Programmer.mro())


# class A:
#     var = 'a'
#
#
# class B(A):
#     var = 'b'
#
#
# class C(A):
#     var = 'c'
#
#
# class D(C, B):
#     # var = 'd'
#     pass
#
#
# d = D()
#
# print(d.var)
#
# print(D.mro())


# class User:
#     name = None
#     age = None
#     city = 'a'
#
#     def __init__(self, new_age=None, new_name=None):
#         if new_age is not None:
#             self.age = new_age
#         if new_name is not None:
#             self.name = new_name
#
#     def say_hello(self):
#         print(f'Hello, I\'m {self.name}, I\'m {self.age} years old')

#
# class Programmer(User):
#     language = None
#     city = 'b'
#
#     def __init__(self, new_age, new_name, lang):
#         super().__init__(new_age, new_name)
#
#         self.language = lang
#
#     def say_hello(self):
#         # super().say_hello()
#         print(f'Hello, I\'m programmer {self.name}, I\'m {self.age} years old, I use {self.language}')


# u1 = User(40, 'Artem_programmer')
# u1.say_hello()

#
# p1 = Programmer(40, 'Artem_programmer', 'python')
# p1.say_hello()


# class User:
#     name = None
#     age = None
#     city = 'a'
#
#     _phone = '123456789'  # protected
#     __passport = 'CH111111'  # private
#
#     def __init__(self, new_age=None, new_name=None):
#         if new_age is not None:
#             self.age = new_age
#         if new_name is not None:
#             self.name = new_name
#
#     def say_hello(self):
#         print(f'Hello, I\'m {self.name}, I\'m {self.age} years old. Phone is {self._phone}, passport {self.__passport}')
#
#     def _foo(self):
#         print('foo')
#
#     def __bar(self):
#         print('bar')
#
#
# u1 = User(40, 'Artem_programmer')
#
# print(u1.age)
# print(u1.name)
# print(u1._phone)
# # print(u1._User__passport)
# u1.say_hello()
#
# u1.age = 'qwerfg'


# HW

# гра "вгадай число"

# 1 программа загадує число (1..10) # random
# 2 користувач вводить число
# 3 програма порівнює числа
# 4 у випадку співпадіня - кінець
# 5 у випадку не співпадіня - на крок 2



# function "вгадай число"

    # 1 программа загадує число (1..10) # random -> function

    # while True:
        # 2 користувач вводить число -> function

        # 3 програма порівнює числа -> function

        # 4 у випадку співпадіня - break
        # 5 у випадку не співпадіня - на крок 2




