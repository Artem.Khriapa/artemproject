#

# def dummy_calc(var1, var2, operation):
#     res = {
#         '+': var1 + var2,
#         '-': var1 - var2,
#     }
#
#     return res.get(operation, 'Invalid operation')

#
# def add(a, b):
#     return a + b
#
#
# def sub(a, b):
#     return a - b
#
#
# def dummy_calc(var1: int | float, var2: int | float, operation: str) -> int | float | str:
#     operations = {
#         '+': add,
#         '-': sub,
#         '*': lambda x, y: x * y,
#         '/': lambda x, y: x / y,
#     }
#
#     func = operations.get(operation)
#
#     if func is not None:
#         return func(var1, var2)
#     else:
#         return 'Invalid operation'


# lambda args, kwargs: smths with args, kwargs

# BAD IDEA

# mul = lambda x, y: x * y
#
# res = mul(10, 20)

# lambda x: str(2 * x)

# func = lambda x: str(2 * x)
#
# def some_f(x):
#     return str(2 * x)
#
# print(func(3))


# recursion

# lst = [1, 2, [3, [4, [5]]], 6, [1, 2, 3]]
#
# lst2 = []
#
# for i in lst:
#     if not isinstance(i, list):
#         lst2.append(i)
#     else:
#         for j in i:
#             if not isinstance(j, list):
#                 lst2.append(j)
#             else:
#                 pass


"""
A() -> A() -> A() ...

A() -> B() -> A() -> B() ...

"""


# 1 1 2 3 5 8 13 21 ....

# n[x] = n[x-1] + n[x-2]

#                f(5)
#          f(4) +     f(3)
#   f(3) +     f(2)    f(2) + f(1)
#f(2) + f(1)


# O(2 ** n)

counter = 0


def fib_rec(n):
    global counter
    counter += 1

    if n <= 2:
        return 1

    return fib_rec(n-1) + fib_rec(n-2)


# print(fib_rec(40))
# print(counter)


# def fib_plain(n):
#     fib1 = 1
#     fib2 = 1
#
#     if n in (1, 2):
#         return fib1
#
#     for _ in range(n - 2):
#         fib1, fib2 = fib2, fib1 + fib2
#
#     return fib2
#
#
# print(fib_plain(1000))


lst = [1, 2, [3, [4, [5]]], 6, [1, 2, 3]]

lst2 = []


def lst_rec(data, lst2):
    for i in data:
        if not isinstance(i, list):
            lst2.append(i)
        else:
            lst_rec(i, lst2)


lst_rec(lst, lst2)

print(lst2)
