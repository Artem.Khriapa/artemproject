# class A:
#     attr1 = 10
#
#     def foo(self):
#         print(self.attr1)
#
#
# a = A()
#
#
# print(a.attr1)
#
#
# attr_name = 'attr1'
# print(getattr(a, attr_name, None))
#
#
# attr_name = 'my_attr1'
# setattr(a, attr_name, 100)  # a.my_attr1 = 100
#
# print(a.my_attr1)
# delattr(a, attr_name)


# class Example:
#
#     my_attribute = 0
#
#     def __getattribute__(self, item):
#         print('in __getattribute__', item)
#         return super().__getattribute__(item)
#
#     def __getattr__(self, item):
#         print('in __getattr__', item)
#         return None
#
#     def __setattr__(self, key, value):
#         print('in __setattr__', key, value)
#         super().__setattr__(key, value)
#
#
#
#
# e = Example()
#
# print(e.my_attribute)
#
# print(e.my_attribute2)
#
# print(getattr(e, 'my_attribute2'))
#
# e.my_attribute2 = 400


# class Example:
#
#     my_attribute = 0
#     my_attribute2 = 0
#
#     def __getitem__(self, item):
#         print('__getitem__', item)
#         return getattr(self, item, None)
#
#     def __setitem__(self, key, value):
#         print('__setitem__', key, value)
#         setattr(self, key, value)
#
#
# e = Example()
#
# print(e['my_attribute2'])  # e.my_attribute2
#
# e['my_attribute2'] = 200  # e.my_attribute2 = 200


# def getitem(obj, item):
#     print('__getitem__', item)
#     return getattr(obj, item, None)
#
#
# def setitem(obj, key, value):
#     print('__setitem__', key, value)
#     setattr(obj, key, value)
#
#
# class Example:
#
#     my_attribute = 0
#     my_attribute2 = 0
#
#     __getitem__ = getitem
#     __setitem__ = setitem
#
#
# e = Example()
#
# print(e['my_attribute2'])  # e.my_attribute2
#
# e['my_attribute2'] = 200  # e.my_attribute2 = 200


# with open('filename.txt', 'a') as file:
#     file.write('hello world')


# file = open('filename.txt', 'at')
#
# file.write('hello world')
#
# file.close()


# with open('filename.txt', 'a') as f:
#     f.write('hello world\n')
#     f.write('hello world\n')
#     f.write('hello world\n')
#     f.write('hello world\n')
#     f.write('hello world\n')


class Example:

    def __enter__(self):
        print('__enter__')
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        print('__exit__', exc_type, exc_val, exc_tb)
        return True


with Example() as ex:
    print('Hello world inside context')
    print(1/0)


print('Hello world outside context')

