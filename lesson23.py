import pandas as pd
#
#
# s = pd.Series(['1', 'd', None, [], [5, 6]])
#
# print(s)
#
# df = pd.DataFrame(
#     {
#         'a': [1, 2, 3, 4],
#         'b': [41, 23, 1, 3],
#     }
# )
#
# print(df)
# print(df.sort_values(by='a', ascending=False))


# df = pd.read_csv('ZIM.csv')
# print(df)


data = {
    'fruits': ['apples', 'oranges', 'cucumbers'],
    'color': ['red', 'orange', 'green'],
}
df = pd.DataFrame(data)

# print(df)
#
# df.to_excel('fruits.xlsx')
