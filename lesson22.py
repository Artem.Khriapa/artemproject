#

"""
a = f1()  # 10**9  core 1
b = f2()  # 10**9  core 2

c = a + b
"""

# I/O

"""
a = f1()  # 10**9  core 1
b = f2()  # 10**9  core 2

c = a + b
"""

'''

-  --  --  job1
 ~~  ~~  ~ job2

'''

# import threading
# import time
#
#
# def threaded_func(name, delay):
#
#     for i in range(10):
#         print(f'{name} - {i}')
#         time.sleep(delay)
#
#
# th1 = threading.Thread(target=threaded_func, args=('function 1', 0.2))
# th2 = threading.Thread(target=threaded_func, args=('function 2', 0.3))
#
# th1.start()
# th2.start()
#
# th1.join()
# th2.join()
#
# print('The end')

#
# import threading
#
#
# counter = 0
#
#
# lock = threading.Lock()
#
#
# def threaded_func():
#
#     global counter
#
#     for i in range(1_000_000):
#         lock.acquire()
#         value = counter
#         value += 1
#         lst = [j for j in range(100)]
#         counter = value
#         lock.release()
#
#
# threads = []
#
# for i in range(5):
#     th = threading.Thread(target=threaded_func)
#     threads.append(th)
#
# for th in threads:
#     th.start()
#
#
# for th in threads:
#     th.join()
#
#
# print('The end', counter)

import time

import requests

import threading


t = time.time()
print(t)


def threaded_func():
    for i in range(5):
        res = requests.get('https://www.python.org/static/img/python-logo.png')

        # print('result', res.content)
        with open(f'py_org_{i}.png', 'wb') as file:
            file.write(res.content)


# threads = []
#
# for i in range(5):
#     th = threading.Thread(target=threaded_func)
#     threads.append(th)
#
# for th in threads:
#     th.start()
#
# for th in threads:
#     th.join()


for i in range(5):
    res = requests.get('https://www.python.org/static/img/python-logo.png')

    # print('result', res.content)
    with open(f'py_org_{i}.png', 'wb') as file:
        file.write(res.content)

print(time.time()-t)


