class User:
    name = 'Bill'
    age = None
    city = None
    _instance = None

    # def __new__(cls, *args, **kwargs):
    #     print('--- in new', *args, **kwargs)
    #     if cls._instance is None:
    #         cls._instance = object.__new__(cls)
    #     return cls._instance

    def __init__(self, new_age, new_name):
        self.age = new_age
        self.name = new_name

    def say_hello(self):
        print(f'Hello, I\'m {self.name}, I\'m {self.age} years old')

    def foo(self):
        print(self.__class__.name)

    @classmethod
    def bar(cls):
        print(cls.name)

    @staticmethod
    def baz():
        print('staticmethod')

    def __bool__(self):
        return False if self.age < 10 else True

    def __str__(self):
        return f'Hello, I\'m {self.name}, I\'m {self.age} years old'

    def __eq__(self, other):
        """ obj == other """
        """ obj != other """
        """ obj > other """
        """ obj < other """
        """ obj >= other """
        """ obj <= other """
        if isinstance(other, self.__class__):
            return self.age == other.age
        else:
            return False

    def __add__(self, other):
        return self.__class__(self.age, other.name)


u = User(20, 'Jane')
u1 = User(30, 'Bob')

print(id(u))
print(id(u1))

print(bool(u))
print(bool(u1))

s = str(u1)
print(u1)

print(u == u1)

res = u + u1

print(res)



