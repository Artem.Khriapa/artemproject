# lst = [1, 2, 3]
#
# result = sum(lst, start=3)
# print(result)
#
# lst = ['1', '2', '3']
# res = ''.join(lst)
# print(res)
#
# rng = range(1, 10, 2)
#
# for i in rng:
#     print(i)
#
#
# lst = list(range(1, 10, 2))
# print(lst)


# enum_obj = enumerate('Lorem ipsum')
# for i in enum_obj:
#     print(i)
#
# res = dict(enumerate('Lorem ipsum'))  # {0: L}
# print(res)


# def foo(val):
#     return val % 3


# for i in range(10):
#     print(foo(i))


# for i in range(10):
#     i = foo(i)
#     print(i)

# for i in map(foo, range(10)):
#     print(i)


# data1 = {1, 2, 3, 4, 5, 6, 7, 8}
# data2 = 'qwer'
#
#
# # for i in range(min(len(data2), len(data1))):
# #     print(data1[i], data2[i])
#
# for i in zip(data1, data2, range(0, 10, 2)):
#     print(i)

# data2 = 'qwertyuio'
#
# rev = reversed(data2)
#
# for i in rev:
#     print(i)


# data = {11, 2, 32, 0.4, 5, 6, 12}
# res = sorted(data, key=lambda x: x % 3, reverse=True)
# print(res)

# data = {11, 2, 32, 0.4, 5, 6, 12}
#
# filtered = filter(lambda x: x % 2 == 0, data)
#
# for i in filtered:
#     print(i)

#
# a = 310
# b = 20
# c = 30
#
# #
# # if a > b and b > 0 and c > 0:
# #     print('match')
#
#
# if all([  # and
#     a > b,
#     b > 0,
#     c > 0,
# ]):
#     print('match')
#
# conditions = [
#     a > b,
#     b > 0,
#     c > 0,
# ]
#
# if all(conditions):
#     print('match')
#
#
# conditions = all([
#     a > b,
#     b > 0,
#     c > 0,
# ])
#
# if conditions:
#     print('match')
#
#
# conditions = any([  # or
#     a > b,
#     b > 0,
#     c > 0,
# ])
#
# if conditions:
#     print('match')


# import lesson11
#
# lesson11.example_function()
# print(lesson11.var2)


# import random
#
# import requests
#
# from lesson11 import example_function, var2
#
# example_function()
# print(var2)
#
# print(random.randint(0, 100))


from lesson11 import example_function, var2   # python3 lesson11.py

print(__name__)

example_function()


