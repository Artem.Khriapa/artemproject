# a = 10
# b = 20
# c = a + b
#
# print(c)
#
# s = '123456'
# b = True
#
# some_counter = 100  # snake case
#
# print(a)
# print(some_counter)


# variable_1 = 10
# variable_2 = 20
#
# c = variable_1 + variable_2

# o O I l


# variable_1 = 10
# variable_1 += 1
#
# print(type(variable_1))
#
# variable_1 = 'sdfghjk'
# variable_1 += 1
# print(type(variable_1))

# my_int = 10
# my_float = 10.0
# print(my_int + my_float)
#
# my_int1 = 20
# my_int2 = 10
#
# res = my_int1 + my_int2
# print(res)
#
# res = my_int1 - my_int2
# print(res)
#
# res = my_int1 * my_int2
# print(res)
#
# res = my_int1 ** my_int2
# print(res)
#
# res = my_int1 ** 0.5
# print(res)
#
# res = my_int1 / my_int2
# print('/', res)
#
# res = 7 // 3
# print('//', res)
#
# res = 8 % 3
# print('%', res)
#
# # res = my_int1 / 0
# # print('/', res)

# my_int1 = 1
#
# my_int1 = my_int1 + (1 + my_int1) * 3
#
# my_int1 += 1  # my_int1 = my_int1 + 1
# my_int1 -= 1  # my_int1 = my_int1 - 1
# my_int1 *= 1  # my_int1 = my_int1 - 1

# my_float = 1.000000000000001
# print(my_float)

# my_float1 = 0.1
# my_float2 = 0.2
#
# print(my_float1 + my_float2)
# res = my_float1 + my_float2
#
#
# print(res == 0.3)
#
# my_bool = True
# my_bool = False
#
# print(1 > 2)
# print(1 < 2)
# print(1 == 2)
# print(1 != 2)
# print(1 >= 2)
# print(1 <= 2)

# my_int = 11234
#
# my_float = float(my_int)
# print(my_float)
#
# my_float = 3.14
# my_int = int(my_float)
# print(my_int)
#
# my_int = int('123456')
# print(my_int)
#
# my_float = float('123456')
# print(my_float)

my_int = 1234

print(bool(0.00000000000000000000000000001))

print(0.00000000000000000000000000001)

my_float = 1e-29  # 1 * 10 ** -29
my_float += 1
print(my_float)

# 1h
